import os
from sqlalchemy import create_engine
from dotenv import load_dotenv

# Load environment variables
load_dotenv()

username = os.getenv('DB_USERNAME', 'user')
password = os.getenv('DB_PASSWORD', 'secret')
database = os.getenv('DB_NAME', 'db')
host = os.getenv('DB_HOST', 'localhost')
port = os.getenv('DB_PORT', '3306')

# Create database engine
db_engine_url = f'mysql+mysqlconnector://{username}:{password}@{host}:{port}/{database}'
db_engine = create_engine(db_engine_url)
