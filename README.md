# EFICHE MEDICINE MIGRATION

This project aims to match new medicine names to old medicine names using fuzzy matching techniques. It utilizes Python libraries like pandas, sqlalchemy, fuzzywuzzy, and nltk to achieve this goal.

**Prerequisites:**

- Python 3.6 or later
- MySQL database

**Installation:**

1. **Clone the repository:**

    ```bash
    git clone https://github.com/username/repository.git
    
    ```

2. **Navigate to the project directory:**

    ```bash
    cd your-repository
    
    ```

3. **Create a virtual environment (recommended):**

    ```bash
    python3 -m venv venv
    source venv/bin/activate  # For Linux/macOS
    venv\Scripts\activate  # For Windows
    
    ```

4. **Install the required libraries:**

    ```bash
    pip install -r requirements.txt
    
    ```

**Configuration:**

1. **Database Credentials:**
    - Create a `.env` file in the project root directory.
    - Add the following environment variables with your MySQL database credentials:

        ```env
        DB_USERNAME=your_username
        DB_PASSWORD=your_password
        DB_NAME=your_database_name
        DB_HOST=your_database_host
        DB_PORT=your_database_port
        
        ```

2. **NLTK Data:**
    - Run the following commands once to download the necessary NLTK resources:

        ```bash
        python3
        >>> import nltk
        >>> nltk.download('punkt')
        >>> nltk.download('wordnet')
        
        ```

**Usage:**

1. **Run the scripts:**

    ```bash
    python match.py --max 10 # Generates a matches.csv file
    python migrate.py --file matches.csv --start '2024-04-01' --end '2024-04-15' # Updates the database using the generated csv file
    ```

2. **Output:**
    - The script will generate a CSV file named `medicine_matches.csv` in the project directory.
    - This file will contain the matching results, including:
        - New Med ID
        - New Med Name
        - New Med Dosage
        - Matched Old Med Name
        - Matched Old Med ID
        - Match Score

**Customization:**

- **Fuzzy Matching Parameters:** Explore different scorers and score cutoffs within the `find_matches` function to optimize accuracy for your data.
- **Preprocessing:** Adjust the `preprocess_text` function to refine the text normalization and cleaning steps as needed.

**Notes:**

- Ensure that the `medicine_defaults` and `medicines_defaults` tables exist in your MySQL database with the expected data.
- Review the code and comments for a better understanding of the implementation.
