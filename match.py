import re
from time import asctime
import pandas as pd
from fuzzywuzzy import process, fuzz
import logging
import nltk
from nltk.stem import WordNetLemmatizer
from database import db_engine
import argparse

# Download NLTK resources (run once)
# nltk.download('punkt', quiet=True)
# nltk.download('wordnet', quiet=True)

def preprocess_text(text):
    """Preprocesses text data for improved fuzzy matching."""
    text = text.lower()
    text = re.sub(r"[^\w\s]", "", text)
    tokens = nltk.word_tokenize(text)
    lemmatizer = WordNetLemmatizer()
    lemmas = [lemmatizer.lemmatize(token) for token in tokens]
    return " ".join(lemmas)


def find_matches(new_name, old_names_with_ids, limit, scorer=fuzz.token_sort_ratio):
    """Finds fuzzy matches for a new medicine name and returns match scores and old medicine IDs."""
    matches = process.extract(new_name, old_names_with_ids, limit=limit, scorer=scorer)
    filtered_matches = [(match[1], old_id) for match, old_id in matches][:limit]
    return filtered_matches


def main(max):
    """Matches new medicine names to old names using fuzzy matching and outputs results to a CSV."""
    if not max:
        logging.error("No maximum matches provided, e.g. 50")
        return

    confirm = input(
        f"This operation will generate {max} matches for every new medicine listed. Are you sure you want to proceed? (yes/no) "
    )
    if confirm.lower() != "yes":
        logging.info("Operation cancelled by user.")
        return

    # Setup logging
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
    )

    # Define new medicine IDs
    med_ids = [
        23607,
        23533,
        23570,
        23644,
        23459,
        23422,
        21424,
        12174,
        12063,
        18353,
        18316,
        19870,
        14579,
    ]

    # Fetch medicine data with IDs
    new_meds_query = "SELECT id, name, dosage FROM medicine_defaults WHERE id IN ({})"
    old_meds_query = "SELECT id, name FROM medicines_defaults limit 50000"
    new_meds = pd.read_sql(
        new_meds_query.format(",".join(map(str, med_ids))), db_engine
    )
    old_meds = pd.read_sql(old_meds_query, db_engine)

    # Preprocess medicine names
    new_meds["name_with_dosage"] = new_meds.apply(
        lambda row: preprocess_text(f"{row['name']} {row['dosage']}".strip()), axis=1
    )
    old_meds["name_preprocessed"] = old_meds["name"].apply(preprocess_text)
    old_names_with_ids = list(zip(old_meds["name_preprocessed"], old_meds["id"]))

    # Find matches and store results
    new_meds["matches"] = new_meds["name_with_dosage"].apply(
        lambda x: find_matches(
            x, old_names_with_ids, limit=max, scorer=fuzz.token_sort_ratio
        )
    )
    matches = dict(zip(new_meds["id"], new_meds["matches"]))

    # Prepare data for CSV export
    flattened_data = []
    for new_id, match_list in matches.items():
        if match_list:
            for old_id, score in match_list:
                flattened_data.append(
                    {
                        "new_med_name": new_meds.loc[
                            new_meds["id"] == new_id, "name"
                        ].iloc[0]
                        + " "
                        + new_meds.loc[new_meds["id"] == new_id, "dosage"].iloc[0],
                        "old_med_name": old_meds.loc[
                            old_meds["id"] == old_id, "name"
                        ].iloc[0],
                        "new_med_id": new_id,
                        "old_med_id": old_id,
                    }
                )

    # Export matches to CSV
    matches_df = pd.DataFrame(flattened_data)
    filename = "matches.csv"
    matches_df.to_csv(filename, index=False)
    logging.info(f"Exported medicine matches to ./{filename}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Matches new medicine names to old names using fuzzy matching and outputs results to a CSV."
    )
    parser.add_argument(
        "--max",
        type=int,
        required=True,
        help="The maximum number of possible matches per one new medicine.",
    )
    args = parser.parse_args()
    main(args.max)
