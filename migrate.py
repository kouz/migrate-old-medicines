import pandas as pd
import logging
from database import db_engine
from sqlalchemy import text
import argparse
from datetime import datetime


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d").date()
    except ValueError:
        raise argparse.ArgumentTypeError(
            "Not a valid date: '{0}'. Use YYYY-MM-DD format.".format(s)
        )


def main(file, start, end):
    """Save validated medicine matches in inventory."""

    if not file:
        logging.error("No file path provided.")
        return

    if not start:
        logging.error("No start date provided.")
        return

    if not end:
        logging.error("No end date provided.")
        return

    confirm = input(
        "This operation will create new records in the connected database based on the VALIDATED matches in the file. Are you sure you want to proceed? (yes/no) "
    )
    if confirm.lower() != "yes":
        logging.info("Operation cancelled by user.")
        return

    # parse matches.csv file into a df
    matches_df = pd.read_csv(file)
    matches = matches_df.itertuples()
    total_matches = len(matches_df)

    for match in matches:
        # get all past prescriptions for matched old medicines before 2024-04-14,
        # from table visit_prescriptions, with columns: medicine_id, quantity, and facility_id
        med_prescriptions_qry = f"""
          SELECT medicine_id, quantity, visit_id, v.facility_id, v.created_at, v.updated_at
          FROM visit_prescriptions vp
          JOIN visits v ON vp.visit_id = v.id
          WHERE vp.medicine_id = {match.old_med_id} AND quantity > 0
          AND v.created_at BETWEEN '{start}' AND '{end}'
          ORDER BY v.created_at ASC
        """
        med_prescriptions_df = pd.read_sql(med_prescriptions_qry, db_engine)
        med_prescriptions = med_prescriptions_df.itertuples()
        total_med_prescriptions = len(med_prescriptions_df)

        for prescription in med_prescriptions:
            # progress on prescriptions
            print(
                f"processing match {match.Index + 1} of {total_matches} matches / prescription {prescription.Index + 1} of {total_med_prescriptions} prescriptions",
                end="\r",
                flush=True,
            )

            prescription_year = prescription.created_at.year
            prescription_month = prescription.created_at.month

            # get the stock
            med_stock_qry = f"""
              SELECT id, price, stock_qty
              FROM medicine_stock
              WHERE medicine_id={match.new_med_id} AND facility_id={prescription.facility_id}
              LIMIT 1
            """
            med_stock_df = pd.read_sql(med_stock_qry, db_engine)

            # if there is no stock, create new one
            if med_stock_df.empty:
                logging.warn(
                    f"No stock entry found for medicine_id={match.new_med_id} AND facility_id={prescription.facility_id}, skipping ..."
                )
                continue

            med_stock = med_stock_df.iloc[0]

            # get the last inventory record in the same month
            med_last_inventory_qry = f"""
              SELECT type, action, previous_quantity, quantity, created_at
              FROM medicine_inventory
              WHERE medicine_id={med_stock.id}
              AND facility_id={prescription.facility_id}
              AND YEAR(created_at) = {prescription_year}
              AND MONTH(created_at) = {prescription_month}
              AND created_at <= '{end}'
              ORDER BY created_at DESC
              LIMIT 1
            """
            med_last_inventory_df = pd.read_sql(med_last_inventory_qry, db_engine)

            # add prescription to inventory
            # if the medecine's last inventory has a "previous quantity" in the same month, then:
            if med_last_inventory_df.empty == False:
                med_last_inventory = med_last_inventory_df.iloc[0]

                # subtract the "prescribed quantity" from that "previous quantity", and store the result as the new record's "previous quantity"
                # and set the new "quantity" to the prescribed quantity
                if med_last_inventory.type == "received":
                    previous_quantity = med_last_inventory.quantity or 0
                else:
                    previous_quantity = med_last_inventory.previous_quantity - (
                        med_last_inventory.quantity or 0
                    )

                with db_engine.connect() as db_connection:
                    new_med_inventory_qry = f"""
                      INSERT INTO medicine_inventory(medicine_id, facility_id, type, previous_quantity, quantity, visit_id, previous_price, price, created_at, updated_at)
                      VALUES({med_stock.id}, '{prescription.facility_id}', 'dispensed', {previous_quantity}, {prescription.quantity}, {prescription.visit_id}, {med_stock.price}, {med_stock.price}, '{prescription.created_at}', '{prescription.updated_at}')
                    """
                    db_connection.execute(text(new_med_inventory_qry))
                    db_connection.commit()
                    db_connection.close()
            else:
                # else, calculate the quantity at the start of the month and end of the month for that medicine,
                med_month_total_qty_qry = f"""
                    SELECT SUM(vp.quantity) as total_qty
                    FROM visit_prescriptions vp
                    JOIN visits v on vp.visit_id = v.id
                    WHERE vp.medicine_id={match.old_med_id} AND v.facility_id={prescription.facility_id}
                    AND YEAR(v.created_at) = {prescription_year}
                    AND MONTH(v.created_at) = {prescription_month}
                    AND v.created_at <= '{end}'
                    GROUP BY vp.medicine_id
                """
                med_month_total_qty_df = pd.read_sql(med_month_total_qty_qry, db_engine)
                med_month_total_qty = (
                    next(med_month_total_qty_df.itertuples()).total_qty or 0
                )

                with db_engine.connect() as db_connection:
                    # add received inventory
                    new_med_inventory_qry = f"""
                      INSERT INTO medicine_inventory(medicine_id, facility_id, type, action, previous_quantity, quantity, previous_price, price, created_at, updated_at)
                      VALUES({med_stock.id}, '{prescription.facility_id}', 'received', 'updated', 0, {med_month_total_qty}, {med_stock.price}, {med_stock.price}, '{prescription.created_at}', '{prescription.updated_at}')
                    """
                    db_connection.execute(text(new_med_inventory_qry))
                    db_connection.commit()
                    db_connection.close()

                with db_engine.connect() as db_connection:
                    # add new inventory
                    med_last_inventory_df = pd.read_sql(
                        med_last_inventory_qry, db_engine
                    )
                    med_last_inventory = med_last_inventory_df.iloc[0]
                    if med_last_inventory.type == "received":
                        previous_quantity = med_last_inventory.quantity or 0
                    else:
                        previous_quantity = med_last_inventory.previous_quantity - (
                            med_last_inventory.quantity or 0
                        )
                        new_med_inventory_qry = f"""
                          INSERT INTO medicine_inventory(medicine_id, facility_id, type, previous_quantity, quantity, visit_id, previous_price, price, created_at, updated_at)
                          VALUES({med_stock.id}, '{prescription.facility_id}', 'dispensed', {previous_quantity}, {prescription.quantity}, {prescription.visit_id}, {med_stock.price}, {med_stock.price}, '{prescription.created_at}', '{prescription.updated_at}')
                        """
                        db_connection.execute(text(new_med_inventory_qry))
                        db_connection.commit()
                        db_connection.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Process a file containing medicine matches. Run only once, running more than once duplicates data"
    )
    parser.add_argument(
        "--file",
        type=str,
        required=True,
        help="The path to the standardised CSV file containing VALIDATED matches of old medicine to new medicines.",
    )
    parser.add_argument(
        "--start",
        type=valid_date,
        required=True,
        help="Start date of records to recover",
    )
    parser.add_argument(
        "--end",
        type=valid_date,
        required=True,
        help="End date of records to recover",
    )
    args = parser.parse_args()
    main(args.file, args.start, args.end)
